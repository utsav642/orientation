# Git Summary
### what I understood...

>What I understood by git is that,  
it is a **platform for programmers** to collaborate and to make  
working and tracking each and every developements over there.

I won't go into the much details but you come across various  
functions like **commit, fork, clone, repository, issues, wiki, merge,**  
**pull requests, push, etc**.
![add-commit-push changes](https://gitlab.com/utsav642/orientation/-/raw/master/extras/gitlab.png)

Git is a journey straight from your workspace to the remote git  
repository(repo) on the internet.

Repository is basically, the space where all the **collaborators** come and  
throw the code files and all the work.

cloning is exactly same as what it sounds like. you import the  
same repository from remote space to your **machine(local repository)**.

fork means creating the duplicate repository under your name.

in issues section you look for the common problem statements  
and work on it.

we can handle the git by **command-line** arguments.  
![image-of-terminal](https://gitlab.com/utsav642/orientation/-/raw/master/extras/summary.png)
 
